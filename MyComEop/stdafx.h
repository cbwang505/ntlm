// stdafx.h : 标准系统包含文件的包含文件，
// 或是经常使用但不常更改的
// 特定于项目的包含文件
//

#ifdef ARRAYSIZE
#define countof(a) (ARRAYSIZE(a)) // (sizeof((a))/(sizeof(*(a))))
#else
#define countof(a) (sizeof((a)) / (sizeof(*(a))))

#endif






#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
#define RPC_USE_NATIVE_WCHAR



#pragma once

#include "targetver.h"


#include <stdio.h>
#include <tchar.h>
#include <windows.h>


